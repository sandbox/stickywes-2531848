<?php
/**
 * @file
 * date_field_testing_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function date_field_testing_feature_node_info() {
  $items = array(
    'date_field_testing_node' => array(
      'name' => t('Date Field Testing Node'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
